#include "xnvctrlpp.hpp"

XNvCtrlPP::XNvCtrlPP()
    : display_{Xorg_lib::XOpenDisplay(0)}
{ }

XNvCtrlPP::XNvCtrlPP(const char* display_name)
    : display_{Xorg_lib::XOpenDisplay(display_name)}
{ }

XNvCtrlPP::~XNvCtrlPP()
{
    XCloseDisplay(display_);
}

std::string XNvCtrlPP::get_GPU_name()
{
    char* ptr {read_string_attribute(NV_CTRL_STRING_PRODUCT_NAME)};
    std::string ret {ptr};
    Xorg_lib::XFree(ptr);
    return ret;
}

std::string XNvCtrlPP::get_GPU_VBIOS_version()
{
    char* ptr {read_string_attribute(NV_CTRL_STRING_VBIOS_VERSION)};
    std::string ret {ptr};
    Xorg_lib::XFree(ptr);
    return ret;
}

std::string XNvCtrlPP::get_GPU_UUID()
{
    char* ptr {read_string_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_STRING_GPU_UUID)};
    std::string ret {ptr};
    Xorg_lib::XFree(ptr);
    return ret;
}

std::string XNvCtrlPP::get_GPU_driver_version()
{
    char* ptr {read_string_attribute(NV_CTRL_STRING_NVIDIA_DRIVER_VERSION)};
    std::string ret {ptr};
    Xorg_lib::XFree(ptr);
    return ret;
}

std::string XNvCtrlPP::get_GPU_current_clock_freq()
{
    char* ptr {read_string_attribute(NV_CTRL_STRING_GPU_CURRENT_CLOCK_FREQS)};
    std::string ret {ptr};
    Xorg_lib::XFree(ptr);
    return ret;
}

unsigned XNvCtrlPP::get_GPU_total_memory() const          { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_TOTAL_GPU_MEMORY); }
unsigned XNvCtrlPP::get_GPU_slowdown_threshold() const    { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_GPU_SLOWDOWN_THRESHOLD); }
unsigned XNvCtrlPP::get_GPU_shutdown_threshold() const    { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_GPU_SHUTDOWN_THRESHOLD); }
unsigned XNvCtrlPP::get_GPU_pci_bus() const               { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_PCI_BUS); }
unsigned XNvCtrlPP::get_GPU_pci_device() const            { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_PCI_DEVICE); }
unsigned XNvCtrlPP::get_GPU_pci_function() const          { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_PCI_FUNCTION); }
unsigned XNvCtrlPP::get_GPU_max_screen_width() const      { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_MAX_SCREEN_WIDTH); }
unsigned XNvCtrlPP::get_GPU_max_screen_height() const     { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_MAX_SCREEN_HEIGHT); }
unsigned XNvCtrlPP::get_GPU_max_displays() const          { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_MAX_DISPLAYS); }

unsigned XNvCtrlPP::get_GPU_power_mizer_mode() const      { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_GPU_POWER_MIZER_MODE); }
unsigned XNvCtrlPP::get_GPU_core_temperature() const      { return read_int_attribute(NV_CTRL_TARGET_TYPE_GPU, NV_CTRL_GPU_CORE_TEMPERATURE); }
unsigned XNvCtrlPP::get_GPU_fan_speed_level() const       { return read_int_attribute(NV_CTRL_TARGET_TYPE_COOLER, NV_CTRL_THERMAL_COOLER_CURRENT_LEVEL); }
unsigned XNvCtrlPP::get_GPU_fan_speed_rpm() const         { return read_int_attribute(NV_CTRL_TARGET_TYPE_COOLER, NV_CTRL_THERMAL_COOLER_SPEED); }

XNvCtrlPP::GPU_static_info XNvCtrlPP::get_GPU_static_info()
{
    GPU_static_info static_info {
        /*.name =*/                 get_GPU_name(),
        /*.uuid =*/                 get_GPU_UUID(),
        /*.vbios_ver =*/            get_GPU_VBIOS_version(),
        /*.driver_ver =*/           get_GPU_driver_version(),
        /*.total_memory =*/         get_GPU_total_memory(),
        /*.slowdown_temperature =*/ get_GPU_slowdown_threshold(),
        /*.shutdown_temperature =*/ get_GPU_shutdown_threshold(),
        /*.pci_bus =*/              get_GPU_pci_bus(),
        /*.pci_device =*/           get_GPU_pci_device(),
        /*.pci_function =*/         get_GPU_pci_function(),
        /*.max_screen_width =*/     get_GPU_max_screen_width(),
        /*.max_screen_height =*/    get_GPU_max_screen_height(),
        /*.max_displays =*/         get_GPU_max_displays()
    };
    return static_info;
}

XNvCtrlPP::GPU_dynamic_info XNvCtrlPP::get_GPU_dynamic_info()
{
    GPU_dynamic_info dymamic_info {
        /*.power_mizer_mode =*/     get_GPU_power_mizer_mode(),
        /*.core_temperature =*/     get_GPU_core_temperature(),
        /*.fan_speed_level =*/      get_GPU_fan_speed_level(),
        /*.fan_speed_rpm =*/        get_GPU_fan_speed_rpm(),
    };
    return dymamic_info;
}

char* XNvCtrlPP::read_string_attribute(unsigned attribute_id)
{
    char* ptr {nullptr};
    int screen {XDefaultScreen(display_)};
    Xorg_lib::XNVCtrl::XNVCTRLQueryStringAttribute(display_, screen, ATTRIBUTE_TYPE_READ, attribute_id, &ptr);
    return ptr;
}

char* XNvCtrlPP::read_string_attribute(unsigned target_type, unsigned attribute_id)
{
    char* ptr {nullptr};
    int screen {XDefaultScreen(display_)};
    Xorg_lib::XNVCtrl::XNVCTRLQueryTargetStringAttribute(display_, target_type, screen, ATTRIBUTE_TYPE_READ, attribute_id, &ptr);
    return ptr;
}

int XNvCtrlPP::read_int_attribute(unsigned attribute_id) const
{
    int value {0};
    int screen {Xorg_lib::XDefaultScreen(display_)};
    Xorg_lib::XNVCtrl::XNVCTRLQueryAttribute(display_, screen, ATTRIBUTE_TYPE_READ, attribute_id, &value);
    return value;
}

int XNvCtrlPP::read_int_attribute(unsigned target_type, unsigned attribute_id) const
{
    int value {0};
    int screen {Xorg_lib::XDefaultScreen(display_)};
    Xorg_lib::XNVCtrl::XNVCTRLQueryTargetAttribute(display_, target_type, screen, ATTRIBUTE_TYPE_READ, attribute_id, &value);
    return value;
}
